#include "sys_xintegrity.h"
#include "sys_xintegrity_helper.h"
#include <linux/stat.h>
#include <asm/fcntl.h>

#define CRED_PASSWORD "password"

int processMode1(void *arg);
int processMode2(void *arg);
int processMode3(void *arg);

asmlinkage extern long (*sysptr)(void *arg);

asmlinkage long xintegrity(void *arg)
{
	unsigned char mode;
	int retVal = 0;

	if (!arg)
		return -EINVAL;
	
	/*first check if we can access mode flag */
	if(!access_ok(VERIFY_READ, arg, sizeof(unsigned char))){
		return -EINVAL;		/*Unable to access user arguments*/
	}		
	
	
	/*Read syscall mode first*/
	if(copy_from_user(&mode, arg, sizeof(unsigned char))){
                return -EINVAL; 	/* Invalid user arguments */
	}
	
	/* Process mode*/
	switch(mode){
		case 1:
		retVal = processMode1(arg);
		break;

		case 2:
		retVal = processMode2(arg);
		break;
		
		case 3:
		retVal = processMode3(arg);
		break;
	}
	
	return retVal;
}

/*
 * Processes user arguments for mode1. 
 * 
 * Fetches  md5 checksum of file from extended attribute 
 * and puts it in ibuf field of user argument. 
 * 
 * Returns 0 on success. Error no. on failure.
 */
int processMode1(void *arg){
	int retVal = 0;
	struct sys_integrity_mode1 *input = NULL;
	struct sys_integrity_mode1 *userarg = (struct sys_integrity_mode1 *)arg;
	int mode1_struct_size = sizeof(struct sys_integrity_mode1);
	struct file *filp = NULL;
        mm_segment_t oldfs;
        int bytes;
	int done_get_name = 0;

	if(!access_ok(VERIFY_READ, arg, mode1_struct_size)){
		retVal = -EINVAL;	/* Unable to access user arguments*/
		goto out;
	}
	
	input = kmalloc(mode1_struct_size, GFP_KERNEL);
	
	if(!input){
		retVal = -ENOMEM;	/* Unable to allocate memory */
		goto out;
	}

	if(copy_from_user(input, arg, mode1_struct_size)){
              	retVal = -EINVAL;	/* Unable to copy arguments from user space to kernel*/
		goto out;
        }

	if(userarg->ilen != MD5_HASH_SIZE){
		retVal = -EINVAL;	/* ibuf length should be MD%_HASH_SIZE*/
		goto out;
	}
	
	if(!access_ok(VERIFY_READ,userarg->filename, 32)){
                retVal = -EACCES;
		goto out;
        }
	
	input->filename = getname(userarg->filename); /* Get filename from user space*/
 	done_get_name = 1;		/*Set this flag to check if putname is required*/

        if(!input->filename){
               retVal = -EINVAL;	/* Invalid filename */
	       goto out;
        }
	
	if(!access_ok(VERIFY_WRITE,userarg->ibuf, userarg->ilen)){
		retVal = -EACCES;	/* Can't write to ibuf field */
                goto out;
        }

        filp = filp_open(input->filename, O_RDONLY, 0);
        if (!filp || IS_ERR(filp)) {
                retVal = (int)PTR_ERR(filp);  /* Error opening file */
		goto out;
        }

        oldfs = get_fs();		
        set_fs(KERNEL_DS);

	/* Get existing checksum of file */
	bytes = vfs_getxattr(filp->f_dentry, MD5_XATTR, input->ibuf, MD5_HASH_SIZE);
	
	if(bytes <= 0){
		retVal = -EINVAL;	/* checksum not found for file*/
		set_fs(oldfs);
		goto out;
	}

	input->ilen = bytes;
	set_fs(oldfs);
			
	/* copy checksum to ibuf field in user space*/
	
	if(copy_to_user ((void *)(userarg->ibuf), (void *)(input->ibuf), input->ilen)){
		retVal = -EINVAL;
		goto out;
	}
	
	/* copy input argument to user space so that ilen also gets updated*/
	if(copy_to_user(arg, (void *)input, mode1_struct_size)){
		retVal = -EINVAL;
		goto out;
	}

	out:
	if(!IS_ERR(filp) && filp)
		filp_close(filp, NULL);
	if(!input) {
		if(!(input->ibuf))
			kfree(input->ibuf);
		if(done_get_name)
			putname(userarg->filename);
		kfree(input);
	}
	
	return retVal;
}

/*
 * Process user arguments for mode2.
 * 
 * Calculates checksum of given file and adds
 * it as extended attribute of file. Also returns
 * the checksum to user by populating ibuf field.
 *
 * Returns 0 on success. Error no. on failure.
 */

int processMode2(void *arg){
	int retVal = 0;
        struct sys_integrity_mode2 *input = NULL;
        struct sys_integrity_mode2 *userarg;
	unsigned char *out_buf = NULL;
	int mode2_struct_size = sizeof(struct sys_integrity_mode2);
	int ret = 0, i;
	int done_get_name_file = 0, done_get_name_cred = 0;

	userarg = (struct sys_integrity_mode2 *)arg;
	
	if(!access_ok(VERIFY_READ, arg, mode2_struct_size)){
                retVal = -EACCES;	/* Cant read uesr args*/
                goto out;
        }

        input = kmalloc(mode2_struct_size, GFP_KERNEL);

        if(!input){
                retVal = -ENOMEM;	/* Cant allocate memory*/
                goto out;
        }

        if(copy_from_user(input, arg, mode2_struct_size)){
                retVal = -EINVAL;	/* Unable to copy user args to kernel space*/
                goto out;
        }
	
	if(!input->filename || !input->ibuf || !input->credbuf || input->clen == 0 || input->ilen != MD5_HASH_SIZE){
		retVal = -EINVAL;	/* User passed invalid arguments*/
		goto out;
	}	

        if(!access_ok(VERIFY_READ,userarg->filename, 32)){
                retVal = -EACCES;	/* Unable to access filename*/
                goto out;
        }

        input->filename = getname(userarg->filename);
	done_get_name_file = 1;	/* Set it to check if putname is required*/

        if(!input->filename){
               retVal = -EINVAL;	/* Unable to get file name*/
	       goto out;
        }

	if(!access_ok(VERIFY_WRITE,userarg->ibuf, userarg->ilen)){
                retVal = -EACCES;	/* Can't write to ibuf field in user space*/
                goto out;
        }
	
	if(!access_ok(VERIFY_READ,userarg->credbuf, userarg->clen)){
                retVal = -EACCES;	/* Can't read credentials for file*/
                goto out;
        }

	input->credbuf = getname(userarg->credbuf);
	done_get_name_cred = 1;
	
	if(!input->credbuf){
		retVal = -EINVAL;	/* Invalid credentials */
		goto out;
	}
	
	if(strcmp(input->credbuf, CRED_PASSWORD) != 0){
		retVal = -EPERM;	/* Credentials don't match*/
		goto out;
	}

	out_buf = kmalloc(sizeof(unsigned char)*MD5_HASH_SIZE, GFP_KERNEL);
	
	if(!out_buf){
		retVal = -ENOMEM;	/* Unable to allocate memory*/
		goto out;
	}
	
	/* Calculate checksum for file and add it as xattr*/
	ret = create_md5checksum(input->filename, out_buf);
	
	if(ret != 0){
		retVal = ret;	/* Error adding checksum to file*/
		goto out;
	}
	
	/* copy checksum to ibuf byte by byte as it could contain a zero*/
	for(i=0;i<MD5_HASH_SIZE;i++)
		(input->ibuf)[i] = out_buf[i];

	if(copy_to_user ((void *)(userarg->ibuf), (void *)input->ibuf, input->ilen)){
                retVal = -EPERM;	/*Unable to copy checksum to user space*/
                goto out;
        }

        if(copy_to_user(arg, (void *)input, mode2_struct_size)){
                retVal = -EPERM;	/* Unable to copy other args to user space*/
                goto out;
        }

        out:
	if(!input) {
		if(!input->ibuf)
			kfree(input->ibuf);
		if(done_get_name_file)
			putname(userarg->filename);
		if(done_get_name_cred)
			putname(userarg->credbuf);
                kfree(input);
	}
	if(out_buf)
		kfree(out_buf);
        return retVal;
}


/*
 * Helper function to check if file already exists.
 */
int file_exists(const char *filename){
	int exists = 1;
	struct file *filp = filp_open(filename, O_RDONLY, 0);
	if(!filp || IS_ERR(filp))
		exists = 0;
	else
		filp_close(filp, NULL);
	printk("Does file exist? %d\n", exists);
	return exists;
}

/*
 * Helper function that sets checksum xattr for
 * empty file 
 */
int set_checksum_attr_for_empty_file(struct file* filp){
	int retVal;
	unsigned char  md5_output_buf[MD5_HASH_SIZE];
	retVal =  get_md5checksum_from_filp(filp, md5_output_buf);
	if(retVal == 0){
		retVal = set_md5checksum_xattr(filp, md5_output_buf);	
	}
	return retVal;
}

/*
 * Validates checksum for mode3. Calculates checksum for file
 * compares it against checksum xattr. 
 * Returns 0 if checksum match. Error no. on failure.
 */

int validate_checksum(struct file* filp){
	char md5_xattr[MD5_HASH_SIZE];
	char md5_output_buf[MD5_HASH_SIZE];
	int nbytes, retVal = 0;
	
	nbytes = vfs_getxattr(filp->f_dentry, MD5_XATTR, md5_xattr, MD5_HASH_SIZE);

	if(nbytes <= 0){
                retVal = -ENOENT;
                filp_close(filp, NULL);
                goto out;
        }

        retVal =  get_md5checksum_from_filp(filp, md5_output_buf);
        
	if(retVal != 0){
                printk("Unable to calculate checksum of file\n");
                filp_close(filp,NULL);
                goto out;
        }
        
	if(strncmp(md5_xattr, md5_output_buf, MD5_HASH_SIZE)!=0){
                retVal = -EPERM;
                filp_close(filp, NULL);
                goto out;
        }
	
	out:
	return retVal;
}

/*
 * Process user args for mode3.
 * 
 * Performs integrity check before opening file.
 *
 * Returns file descriptor ( > 2) on success. Error no. on failure.
 */
int processMode3(void *arg){
	int retVal = 0;
	int fd;
        struct sys_integrity_mode3 *input = NULL;
	struct sys_integrity_mode3 *userarg = (struct sys_integrity_mode3 *)arg;
	int mode3_struct_size = sizeof(struct sys_integrity_mode3);
	struct file *filp = NULL;
	int done_get_name = 0;
	int do_checksum_check = 1;

        if(!access_ok(VERIFY_READ, arg, mode3_struct_size)){
                retVal = -EACCES;	/*Can't read user args*/
                goto out;
        }

        input = kmalloc(mode3_struct_size, GFP_KERNEL);

        if(!input){
                retVal = -ENOMEM;	/*Can't allocate memory*/
                goto out;
        }

        if(copy_from_user(input, arg, mode3_struct_size)){
                retVal = -EINVAL;	/*Unable to copy args from user space to kernel*/
                goto out;
        }

	if(!userarg->filename){
		retVal = -EINVAL;	/* Invalid file name*/
		goto out;
	}

        if(!access_ok(VERIFY_READ,userarg->filename, 32)){
                retVal = -EACCES;	/* Unable to read file name */
                goto out;
        }

        input->filename = getname(userarg->filename);
	done_get_name = 1;		/* Set to indicate putname is required*/

        if(!input->filename){
               retVal = -EINVAL;	/* Invalid filename*/
	       goto out;
        }

	if(input->oflag & O_TRUNC) {
		do_checksum_check = 0;	/* Set flag to ignore integrity check of O_TRUNC*/
	}

	if(do_checksum_check && (input->oflag & O_CREAT) && !file_exists(input->filename))
		do_checksum_check = 0;	/* Set flag to ignore integrity check while creating new file*/

	filp = filp_open(input->filename, input->oflag, input->mode);
	if (!filp || IS_ERR(filp)) {
                printk("Error opening file err %d\n", (int) PTR_ERR(filp));
                retVal = (int)PTR_ERR(filp);  
                goto out;
        }
	
	if(do_checksum_check){
		retVal = validate_checksum(filp); /* Validate checksum*/
		if(retVal)
			goto out;
	}
	else{	/* For new file, or while opening file with O_TRUNC, set checksum xattr for empty file*/
		retVal = set_checksum_attr_for_empty_file(filp);
		if(retVal){
			//printk("Unable to set checksum for empty file\n");
			goto out;
		}
	}
	
	fd = get_unused_fd();	/* Get an unused file descriptor*/
	if(fd < 0){
		filp_close(filp, NULL);
                retVal = fd;
                goto out;
        }

	fd_install(fd, filp);	/* Associate file descriptor with file pointer*/
	retVal = fd;
	filp->f_pos = 0;
	
	out:
	if(!input){
		if(done_get_name)
			putname(userarg->filename);
		kfree(input);
	}
	return retVal;
}

static int __init init_sys_xintegrity(void)
{
	printk("installed new sys_xintegrity module\n");
	if (sysptr == NULL)
		sysptr = xintegrity;
	return 0;
}
static void  __exit exit_sys_xintegrity(void)
{
	if (sysptr != NULL)
		sysptr = NULL;
	printk("removed sys_xintegrity module\n");
}
module_init(init_sys_xintegrity);
module_exit(exit_sys_xintegrity);
MODULE_LICENSE("GPL");
