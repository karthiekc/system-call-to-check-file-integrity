#ifndef SYS_XINTEGRITY_H
#define SYS_XINTEGRITY_H

#define MD5_XATTR "user.checksum.md5"
#define MD5_HASH_SIZE 16
struct sys_integrity_mode1 {
        unsigned char flag; // flags to set the mode (1, 2, 3, etc. per mode)
        const char *filename; // the name of the file to verify integrity
        unsigned char *ibuf; // the integrity value (e.g., MD5 value) buffer
        unsigned int ilen; // length of ibuf
};

struct sys_integrity_mode2 {
        unsigned char flag; // flags to set the mode (1, 2, 3, etc. per mode)
        const char *filename; // the name of the file to compute integrity
        unsigned char *ibuf; // the integrity value (e.g., MD5 value) buffer
        unsigned int ilen; // length of ibuf
        unsigned char *credbuf; // credentials buffer
        unsigned int clen; // length of credbuf
};

struct sys_integrity_mode3 {
        unsigned char flag; // flags to set the mode (1, 2, 3, etc. per mode)
        const char *filename; // the name of the file to open+verify
        int oflag; // open flags -- same as open(2)
        int mode; // create mode flags -- same as open(2)
};

#endif
