#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>

#include "sys_xintegrity.h"

#define __NR_xintegrity	349	/* our private syscall number */

/*
 * Function that tests mode 1
 */

int testMode1(const char *fname){
	struct sys_integrity_mode1 input;
	int rc;
	//unsigned char c;
        input.flag = 1;
        input.filename = fname;
        input.ibuf = (unsigned char *)malloc(sizeof(unsigned char)*MD5_HASH_SIZE);
        //input.ibuf = NULL;
	//input.ibuf = &c;
	input.ilen = MD5_HASH_SIZE;
	
	rc = syscall(__NR_xintegrity, (void *)(&input));
	if (rc == 0) {
                if(input.ibuf){
                        printf("checksum size: %d\n", input.ilen);
                        printf("syscall returned checksum:\n");
                        int i=0;
                        while(i<input.ilen){
                                printf("%x", input.ibuf[i++]);
                        }
                        printf("\n");
                }
        }
	
	if(input.ibuf)
        	free(input.ibuf);
	return rc;
}

/*
 * Function that tests mode 2
 */

int testMode2(const char* fname, char *cred){
	struct sys_integrity_mode2 input;
	int rc, i=0;
	//unsigned char c;
        input.flag = 2;
        input.filename = fname;
        input.ibuf = (unsigned char *)malloc(sizeof(unsigned char)*MD5_HASH_SIZE);
        //input.ibuf = NULL;
	input.ilen = MD5_HASH_SIZE;
        //input.credbuf = (unsigned char *)"password";
	input.credbuf = (unsigned char *)cred;
        //input.credbuf = NULL;
	//input.credbuf = &c;
	input.clen = strlen(cred);
	//input.clen = 8;
	rc = syscall(__NR_xintegrity, (void *)(&input));
        if (rc == 0) {
                //printf("syscall returned %d\n", rc);
                 if(input.ibuf){
                        printf("checksum size: %d\n", input.ilen);
                        printf("syscall returned checksum:\n");
                        while(i<input.ilen){
                                printf("%x", input.ibuf[i++]);
                        }
                        printf("\n");
                }
        }

        free(input.ibuf);
        return rc;
}

/*
 * Function that tests mode 3
 */
int testMode3(const char *filename, int file_flags, int file_mode){
	int rc=0;
	//int ret = 0, i = 0;
	//char buf[11];
	struct sys_integrity_mode3 input;
        input.flag = 3;
        input.filename = filename;
        
	//input.oflag = O_TRUNC;//O_RDONLY
	input.oflag = file_flags;
	//input.oflag = O_CREAT;
	//printf("oflag = %d\n",input.oflag);
	input.mode = file_mode;
        //input.mode = 777;//rights

        rc = syscall(__NR_xintegrity, (void *)(&input));
        if(rc > 0){
                printf("Got file descriptor: %d\n", rc);
		
		/*ret = read(rc,buf,10);  
    		if (ret == -1)
    		{
        		printf("Error in reading!\n");
        		rc = -1;
			goto out;
			//exit(0);
    		}
		printf("No. of bytes read: %d\n", ret);
		perror(" read error: ");
		buf[10] = '\0';
		while(i<10){
			printf("%c", buf[i++]);
			
		}
		printf("\n");
		//printf("Read value: %s\n", buf);
		*/
                close(rc);
        }
	else {
		printf("Unable to get correct fd\n");
	}
	return rc;
}

/*
 * Check if user entered a valid mode
 */
int isValidMode(const char *mode){
	int val = atoi(mode);
	return (val >= 1 && val <= 3);
}

int main(int argc, char *argv[])
{
	int rc, mode, file_flags, file_mode;
	char filename[100], password[100] ;
	
	if(argc < 2){
		printf("Too few arguments\n");
		return 0;
	}
	
	if(!isValidMode(argv[1])){
		printf("Invalid mode. Enter a mode between 1 and 3");
		rc = -EINVAL;
		goto out;
	}

	mode = atoi(argv[1]);
   
	switch(mode){
	   case 1:
	      if(argc < 3){
	         printf("Need filename for mode1\n");
		 rc = -EINVAL;
		 goto out;
	      }
	      strcpy(filename, argv[2]);
	      rc = testMode1(filename);
	      break;	
	
	   case 2:
	      if(argc < 4){
	         printf("Need filename and password for mode 2\n");
		 rc = -EINVAL;
		 goto out;	  	
	      }
	      strcpy(filename, argv[2]);
	      strcpy(password, argv[3]);
	      rc = testMode2(filename, password);
	      break;

	   case 3:
	      if(argc < 5){
	         printf("Need filename, open-flags and open-mode for mode 3\n");
		 rc = -EINVAL;
		 goto out;
	      }		
	      strcpy(filename, argv[2]);
	      file_flags = atoi(argv[3]);
	      if(file_flags < 0){
	      	printf("Invalid file open flag\n");
		rc = -EINVAL;
		goto out;
	      }	

	      file_mode = atoi(argv[4]);
	      if(file_mode < 0){
	      	printf("Invalid file open mode\n");
		rc = -EINVAL;
		goto out;
	      }
	      rc = testMode3(filename, file_flags, file_mode);
	      break;
	}

	out:
	perror("Sys call err ");
	exit(rc);
}

