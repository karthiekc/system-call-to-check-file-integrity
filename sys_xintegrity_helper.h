#ifndef SYS_XINTEGRITY_HELPER_H
#define SYS_XINTEGRITY_HELPER_H

#include <linux/linkage.h>
#include <linux/moduleloader.h>
#include <linux/uaccess.h>
#include <linux/fs.h> 
#include <linux/file.h>
#include <linux/slab.h>
#include <linux/xattr.h>
#include <linux/crypto.h>
#include <crypto/hash.h>
#include <linux/scatterlist.h>
#include <linux/fsnotify.h>
#include "sys_xintegrity.h"

#define PAGESIZE 4096

/*forward declaration for function that calculates checksum*/
int do_crypto_and_get_md5checksum(struct scatterlist *sg, struct hash_desc *desc, struct file *filp, unsigned char *output_buf);

/*
 * Gets md5 checksum of file from file pointer.
 * Returns 0 on sucess. Error value on failure.
 * Populates md5 checksum in output_buf. Assumes output_buf
 * has enough space to hold the checksum. 
 */

int get_md5checksum_from_filp(struct file *filp, unsigned char *output_buf){
	int ret, retVal = 0;
	mm_segment_t oldfs = get_fs();
	int hash_len;
	struct crypto_hash *tfm = NULL;
	struct scatterlist sg[1];
	struct hash_desc desc;

	if (!filp || IS_ERR(filp)) {
                printk("read_file err %d\n", (int) PTR_ERR(filp));
                retVal = (int)PTR_ERR(filp);  /* or do something else */
                goto out;
        }

        if (!filp->f_op->read) {
                printk("filesystem doesn't support read\n");
                retVal = -EPERM;  /* file(system) doesn't allow reads */
                goto out;
        }
	
	filp->f_pos = 0;                /* start offset */
        set_fs(KERNEL_DS);
        tfm = crypto_alloc_hash("md5", 0, CRYPTO_ALG_ASYNC);
        if(tfm == NULL){
                printk("Unable to init tfm\n");
                retVal = -ENOMEM;
                goto out;
        }

        hash_len = crypto_hash_digestsize(tfm);
        desc.tfm = tfm;
        ret = crypto_hash_init(&desc);

        if(ret != 0 ){
                retVal = ret;
                goto out;
        }

        sg_init_table(sg, 1);
	
	ret = do_crypto_and_get_md5checksum(sg, &desc, filp, output_buf);
	if(ret != 0){
		retVal = ret;
		goto out;
	}
	
	out:
	set_fs(oldfs);
	if(!tfm){
		crypto_free_hash(tfm);	
	}
	return retVal;
}

/*
 * Function that operates on data structures required by crypto
 * apis and calculates checksum.
 * Returns 0 on success. Error no. of failure.
 * Populates md5 checksum in output_buf.
 */
int do_crypto_and_get_md5checksum(struct scatterlist sg[], struct hash_desc *desc, struct file *filp, unsigned char *output_buf){
	char buf[PAGESIZE];
        int i, buflen = PAGESIZE, bytes, retVal = 0, ret;
        for(i=0;i<buflen;i++){
                buf[i] = 0;
        }
	bytes = filp->f_op->read(filp, buf, buflen, &filp->f_pos);
        while(bytes > 0){
	        sg_set_buf(&sg[0], buf, bytes);
                ret = crypto_hash_update(desc, sg, bytes);
                if(ret != 0){
                        printk("Unable to create hash\n");
                        retVal = -EINVAL;
                        goto out;
                }
                bytes = filp->f_op->read(filp, buf, buflen, &filp->f_pos);
        }
        ret = crypto_hash_final(desc, output_buf);
        if(ret != 0){
                printk("Unable to get hash final\n");
                retVal = -EINVAL;
                goto out;
        }
	out:
	return retVal;
}

/*
 * Sets the xattr for md5 checksum for a given file
 */
int set_md5checksum_xattr(struct file *filp, unsigned char *output_buf){
	int setxattr_flags = 0;
        int retset = vfs_setxattr(filp->f_dentry, MD5_XATTR, output_buf, MD5_HASH_SIZE, setxattr_flags);
        if(retset != 0)
                printk("Unable to set xattr in file\n");
	//else
	//	fsnotify_xattr(filp->f_dentry);
	return retset;
}

/*
 * Calculates md5 check for given file and sets it 
 * as extended attribute to the file.
 * Returns 0 on success. Error no. on failure. 
 * Populates checksum in output_buf.
 */
int create_md5checksum(const char *filename, unsigned char *output_buf){
        int retVal = 0;
        struct file *filp = filp_open(filename, O_RDONLY, 0);
	if(!filp || IS_ERR(filp)){
		printk("error opening file inside create checksum\n");
		retVal = (int)PTR_ERR(filp);
		goto out;
	}
	printk("opened file successfully\n");
	retVal = get_md5checksum_from_filp(filp, output_buf);
	if(retVal != 0)
		goto out;
	retVal = set_md5checksum_xattr(filp, output_buf);
	if(retVal != 0){
		goto out;
	}
	out:
        if(filp && !IS_ERR(filp)){
                filp_close(filp, NULL);
        }
	printk("Returning %d from create_md5checksum\n", retVal);
        return retVal;
}

#endif
